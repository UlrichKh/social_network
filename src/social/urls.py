from django.urls import path

from . import views

app_name = 'social'

urlpatterns = [
    path('', views.index, name='index'),
    # Accounts views
    path('login', views.login_view, name='login'),
    path('logout', views.logout_view, name='logout'),
    path('register', views.register_view, name='register'),

    # Posts views
    path('posts/create', views.create_post, name='create'),
    path('posts/<int:id>', views.toggle_like_model, name='toggle_like'),
]
