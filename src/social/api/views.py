from rest_framework.decorators import api_view
from rest_framework.response import Response

from django.http import JsonResponse
from django.contrib.auth import authenticate, login

from social.models import Like, Post, User


@api_view(['GET'])
def analitics(request):
    response = {}
    date_from = request.GET['date_from']
    date_to = request.GET['date_to']
    likes_list = Like.objects.filter(like_date__range=[date_from, date_to])
    for i, like in enumerate(likes_list):
        response[i] = like.serialize()
    response['total'] = likes_list.count()
    return Response(response)


@api_view(['GET'])
def user_stat(request):
    response = {}
    users = User.objects.all()
    for i, user in enumerate(users):
        response[i] = user.serialize()
    return Response(response)


@api_view(['POST'])
def create_post(request):
    header = request.POST['header']
    text = request.POST['text']
    author = request.user
    Post.objects.create(
        header=header,
        text=text,
        author=author,
    )
    return JsonResponse({'message': f'Post {header} created sucessfully'})


@api_view(['POST'])
def login_view(request):
    email = request.POST["email"]
    password = request.POST["password"]
    user = authenticate(request, username=email, password=password)
    login(request, user)
    return JsonResponse({'message': f'Logged it as {email}'})
