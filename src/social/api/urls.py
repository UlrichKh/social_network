from django.urls import path

from . import views

app_name = 'api'

urlpatterns = [
    path('likes/analitics', views.analitics, name='analitics'),
    path('user/stat', views.user_stat, name='user_stat'),
    path('posts/create', views.create_post, name='create_post'),
    path('user/login_view', views.login_view, name='login'),
]
