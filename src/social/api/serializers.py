from rest_framework import serializers
from social.models import Post, User


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = {
            'id',
            'header',
            'text',
            'author',
            'creation_date',
            'likes_count',
        }


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = {
            'email',
            'password',
            'last_login',
            'last_request',
        }
