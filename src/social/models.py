from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.


class User(AbstractUser):
    last_request = models.DateTimeField(null=True, blank=True)

    def serialize(self):
        return {
            'email': self.email,
            'last_login': self.last_login,
            'last_request': self.last_request
        }


class Post(models.Model):
    header = models.CharField(max_length=512)
    text = models.TextField(max_length=1000)
    creation_date = models.DateTimeField(auto_now_add=True)
    edit_date = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name="author")
    likes = models.ManyToManyField(to=User, related_name="likes")

    @property
    def likes_count(self):
        # count = len(self.likes.all())
        count = self.like_set.all().count()
        return count


class Like(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    like_date = models.DateTimeField(auto_now_add=True)

    def serialize(self):
        return {
            'id': self.id,
            'post': self.post.header,
            'like_date': self.like_date.strftime("%b %-d %Y, %-I:%M %p")
        }
