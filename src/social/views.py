from django.shortcuts import render, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.db import IntegrityError
from django.urls import reverse
from social.forms import PostCreateForm
from django.contrib.auth.decorators import login_required

from social.models import Like, User, Post


# Create your views here.
def index(request):
    return render(request, 'social/index.html', {
        'posts': Post.objects.all(),
    })


def login_view(request):
    if request.method == "POST":
        email = request.POST["email"]
        password = request.POST["password"]
        user = authenticate(request, username=email, password=password)

        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse('social:index'))
        else:
            return render(request, "social/login.html", {
                "message": "Invalid email and/or password."
            })
    else:
        return render(request, 'social/login.html')


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))


def register_view(request):
    if request.method == "POST":
        email = request.POST['email']
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']
        if password != confirm_password:
            return render(request, '')

        try:
            user = User.objects.create_user(email, email, password)
            user.save()
        except IntegrityError as e:
            print(e)
            return render(request, "social/register.html", {
                "message": "Email address already taken."
            })
        login(request, user)
        return HttpResponseRedirect(reverse("social:index"))
    else:
        return render(request, "social/register.html")


def create_post(request):
    if request.method == 'POST':
        form = PostCreateForm(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.author = request.user
            form.save()
            return render(request, "social/index.html", {
                "message": "Post created and published."
            })
        else:
            return render(request, 'social/create.html', {
                'form': form
            })
    else:
        form = PostCreateForm()
        return render(request, 'social/create.html', {
                'form': form
            })


def toggle_like(request, id):
    post = Post.objects.get(id=id)
    user = request.user
    if user in post.likes.all():
        post.likes.remove(user)
    else:
        post.likes.add(user)
    return HttpResponseRedirect(reverse("social:index"))


@login_required(login_url='/social/login/')
def toggle_like_model(request, id):
    post = Post.objects.get(id=id)
    user = request.user
    like, created = Like.objects.get_or_create(
        user=user,
        post=post,
    )
    if created:
        return HttpResponseRedirect(reverse("social:index"))
    else:
        like.delete()
        return HttpResponseRedirect(reverse("social:index"))
